"""Utilities for processing the overall event.

The module contains useful functions for handling data at the event level. More fine-grained utilities are 
reserved for `detector_utils` and `cell_utils`.
    
Todo:
    * Pull module IDs out into a csv file for readability """

# System
import os
import argparse
import logging
import multiprocessing as mp
from functools import partial

# Externals
import yaml
import numpy as np
import pandas as pd
import trackml.dataset

import torch
import itertools

# from .data_utils import Data
from torch_geometric.data import Data


def remap_edges(true_edges, hits):

    unique_hid = np.unique(hits.hit_id)
    hid_mapping = np.zeros(unique_hid.max() + 1).astype(int)
    hid_mapping[unique_hid] = np.arange(len(unique_hid))

    hits = hits.drop_duplicates(subset="hit_id").sort_values("hit_id")
    assert (
        hits.hit_id == unique_hid
    ).all(), "If hit IDs are not sequential, this will mess up graph structure!"

    true_edges = hid_mapping[true_edges]

    return true_edges, hits

# Use get_modulewise_edges from trackml pipeline
def get_modulewise_edges(hits):

    signal = hits[
        ((~hits.particle_id.isna()) & (hits.particle_id != 0)) & (~hits.vx.isna())
    ]
    signal = signal.drop_duplicates(
        subset=["particle_id", "module_id"]
    )

    # Sort by increasing distance from production
    signal = signal.assign(
        R=np.sqrt(
            (signal.tx - signal.vx) ** 2
            + (signal.ty - signal.vy) ** 2
            + (signal.tz - signal.vz) ** 2
        )
    )
    signal = signal.sort_values("R").reset_index(drop=False)

    # Handle re-indexing
    signal = signal.rename(columns={"index": "unsorted_index"}).reset_index(drop=False)
    signal.loc[signal["particle_id"] == 0, "particle_id"] = np.nan

    # Group by particle ID
    signal_list = signal.groupby(["particle_id"], sort=False)["index"].agg(
        lambda x: list(x)
    )

    true_edges = []
    for row in signal_list.values:
        for i, j in zip(row[:-1], row[1:]):
            true_edges.append([i, j])

    true_edges = np.array(true_edges).T

    true_edges = signal.unsorted_index.values[true_edges]
    print(true_edges.shape)

    return true_edges


def calc_eta(r, z):
    theta = np.arctan2(r, z)
    return -1.0 * np.log(np.tan(theta / 2.0))


def select_hits(hits, particles, hits_detector, noise=True, min_pt=0):

    particles = particles[(np.sqrt(particles.px**2 + particles.py**2) > min_pt)]
    
    if noise:
        hits = hits.merge(
            particles[["particle_id", "vx", "vy", "vz"]],
            on="particle_id",
            how="left",
        )
    else:
        hits = hits.merge(
            particles[["particle_id", "vx", "vy", "vz"]],
            on="particle_id",
        )
    
    hits = hits.merge(
        hits_detector[["hit_id", "module_id"]],
        on="hit_id",
    )

    hits = hits.assign(pt=np.sqrt(hits.tpx**2 + hits.tpy**2))

    hits["nhits"] = hits.groupby("particle_id")["particle_id"].transform("count")
    hits.loc[hits.particle_id == 0, "nhits"] = -1

    r = np.sqrt(hits.tx**2 + hits.ty**2)
    phi = np.arctan2(hits.ty, hits.tx)
    eta = calc_eta(r, hits.tz)
    

    # Select the data columns we need
    hits = hits.assign(r=r, phi=phi, eta=eta)

    return hits

def clean_duplicates(hits):

    noise_hits = hits[hits.particle_id == 0].drop_duplicates(subset="hit_id")
    signal_hits = hits[hits.particle_id != 0]

    non_duplicate_noise_hits = noise_hits[~noise_hits.isin(signal_hits.hit_id)]
    hits = pd.concat([signal_hits, non_duplicate_noise_hits], ignore_index=True)

    return hits

def build_event(
    event_file,
    feature_scale,
    cell_features,
    min_pt=0,
    modulewise=True,
    noise=False,
):
    # Get true edge list using the ordering by R' = distance from production vertex of each particle
    particles = pd.read_csv(event_file + "-particles.csv")
    hits = pd.read_csv(event_file + "-truth.csv")
    hits_detector = pd.read_csv(event_file + "-hits.csv")
    
    hits = select_hits(
        hits, particles, hits_detector, noise=noise, min_pt=min_pt,
    ).assign(evtid=int(event_file[-9:]))

    hits = clean_duplicates(hits)

    module_id = hits.module_id.to_numpy()
    
    # Handle which truth graph(s) are being produced
    modulewise_true_edges = None

    if modulewise:
        modulewise_true_edges = get_modulewise_edges(hits)
        logging.info(
            "Modulewise truth graph built for {} with size {}".format(
                event_file, modulewise_true_edges.shape
            )
        )

    # Make a unique module ID and attach to hits
    # module_lookup = pd.read_pickle(os.path.join(os.path.split(os.path.split(event_file)[0])[0], "module_lookup"))
    # hits = hits.merge(module_lookup, on=["hardware", "layer_disk", "eta_module", "phi_module", "barrel_endcap"], how="left")

    logging.info("Weights constructed")

    return (
        hits[["r", "phi", "tz"]].to_numpy() / feature_scale,
        hits.particle_id.to_numpy(),
        modulewise_true_edges,
        hits["hit_id"].to_numpy(),
        hits.pt.to_numpy(),
        hits.nhits.to_numpy(),
        module_id
    )


def prepare_event(
    event_file,
    cell_features,
    progressbar=None,
    output_dir=None,
    modulewise=True,
    noise=False,
    min_pt=0,
    overwrite=False,
    **kwargs
):

    try:
        evtid = int(event_file[-9:])
        filename = os.path.join(output_dir, str(evtid))

        if not os.path.exists(filename) or overwrite:
            logging.info("Preparing event {}".format(evtid))
            feature_scale = [1000, np.pi, 1000]

            (
                X,
                pid,
                modulewise_true_edges,
                hid,
                pt,
                nhits,
                module_id
            ) = build_event(
                event_file,
                feature_scale,
                cell_features,
                modulewise=modulewise,
                noise=noise,
                min_pt=min_pt,
            )

            hit_data = Data(
                x=torch.from_numpy(X).float(),
                pid=torch.from_numpy(pid),
                event_file=event_file,
                hid=torch.from_numpy(hid),
                pt=torch.from_numpy(pt),
                nhits=torch.from_numpy(nhits),
                modules=torch.from_numpy(module_id)
            )
            if modulewise_true_edges is not None:
                hit_data.modulewise_true_edges = torch.from_numpy(modulewise_true_edges)

            with open(filename, "wb") as pickle_file:
                torch.save(hit_data, pickle_file)

        else:
            logging.info("{} already exists".format(evtid))
    except Exception as inst:
        print("Exception with file:", event_file, "Exception:", inst)
