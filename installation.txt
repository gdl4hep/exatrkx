conda env create -n exatrkx -f gpu_environment
conda install -c conda-forge jupyterlab
conda install -c anaconda jupyter (?)
conda install -n exatrkx ipykernel --update-deps --force-reinstall (??)
pip install -e .
conda install -c conda-forge tensorboard (?)