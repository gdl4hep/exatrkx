#!/bin/bash

#SBATCH --job-name=exa.trkx
#SBATCH --nodes=1
#SBATCH --gpus-per-node=1
#SBATCH --time=10080
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err

module purge
module load cuda/11.7                             # Use cuda 11.7   
module load python/anaconda3                      # Load anaconda3
eval "$(conda shell.bash hook)"                   # Initiliaze conda
conda activate exatrkx                            # Activate python environment
NOTEBOOK=Examples/TrackML_Quickstart/run_quickstart.ipynb
jupyter trust $NOTEBOOK
jupyter notebook --port=8888                      # Jupyter