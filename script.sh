# Quickstart

export EXATRKX_DATA=~/Tracking-ML-Exa.TrkX/data

kaggle competitions download \
    -c trackml-particle-identification \
    -f train_sample.zip \
    -p $EXATRKX_DATA
\

kaggle competitions download \
    -c trackml-particle-identification \
    -f detectors.zip \
    -p $EXATRKX_DATA
\

cd $EXATRKX_DATA/; unzip detectors.zip; unzip train_sample.zip; rm *.zip
